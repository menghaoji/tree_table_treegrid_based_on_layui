package kit;

import com.jfinal.plugin.activerecord.Page;

import java.util.List;
import java.util.Map;

public class Kit {
    /**
     * 針對lay封裝分頁對象（page）
     */
    public static  void setLayPage(Map retMap, Page data){
        retMap.put("data", data.getList());
        retMap.put("code", 0);
        retMap.put("msg", "");
        retMap.put("count", data.getTotalRow());
    }

    /**
     * 針對lay封裝對象（tree）
     */
    public static  void setLayTree(Map retMap,List data){
        retMap.put("data", data);
        retMap.put("code", 0);
        retMap.put("msg", "");
        retMap.put("count", data.size());
    }
}
